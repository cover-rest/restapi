from __future__ import annotations

from multipledispatch import dispatch
from bson.objectid import ObjectId
from mongoengine.errors import NotUniqueError, DoesNotExist
from mongoengine.queryset.queryset import QuerySet

from .types.project_type import ProjectDatabaseType
from .types.commit_type import CommitDatabaseType
from .types.coverage_type import CoverageDatabaseType
from .types.coverage_type import SourceDatabaseType
from .types.coverage_type import PackageDatabaseType
from .types.coverage_type import ClassDatabaseType
from .types.coverage_type import MethodeDatabaseType
from ..swagger_server.models.project_type import ProjectType
from ..swagger_server.models.commit_type import CommitType
from ..swagger_server.models.coverage_type import CoverageType
from ..swagger_server.models.source_type import SourceType
from ..swagger_server.models.package_type import PackageType
from ..swagger_server.models.class_type import ClassType
from ..swagger_server.models.methode_type import MethodeType
from ..swagger_server.models.line_type import LineType
from .errors import NotFound, NotUnique, NotAuthorized, MissingInputParameters


class DatabaseInterface:
    @staticmethod
    @dispatch(ObjectId)
    def read_project(project_id: ObjectId) -> ProjectDatabaseType:
        return read_project(project_id=project_id)[0]

    @staticmethod
    @dispatch(str)
    def read_project(project_title_url: str) -> ProjectDatabaseType:
        if _is_url(project_title_url):
            return read_project(project_url=project_title_url)[0]
        else:
            return read_project(project_title=project_title_url)[0]

    @staticmethod
    @dispatch(ProjectDatabaseType)
    def write_project(project: ProjectDatabaseType) -> ObjectId:
        return write_project(project=project)

    @staticmethod
    @dispatch(ObjectId)
    def read_commit(commit_id: ObjectId) -> CommitDatabaseType:
        return read_commit(commit_id=commit_id)[0]

    @staticmethod
    @dispatch(str, ObjectId)
    def read_commit(commit_hash: str, project_id: ObjectId) -> CommitDatabaseType:
        return read_commit(commit_hash=commit_hash, project_id=project_id)[0]

    @staticmethod
    @dispatch(str, str)
    def read_commit(commit_hash: str, project_title_url: str) -> CommitDatabaseType:
        if _is_url(project_title_url):
            return read_commit(commit_hash=commit_hash, project_url=project_title_url)[0]
        else:
            return read_commit(commit_hash=commit_hash, project_title=project_title_url)[0]

    @staticmethod
    @dispatch(ObjectId, CommitDatabaseType)
    def write_commit(project_id: ObjectId, commit: CommitDatabaseType) -> ObjectId:
        return write_commit(project_id=project_id, commit=commit)

    @staticmethod
    @dispatch(str, CommitDatabaseType)
    def write_commit(project_title_url: str, commit: CommitDatabaseType) -> ObjectId:
        if _is_url(project_title_url):
            return write_commit(project_url=project_title_url, commit=commit)
        else:
            return write_commit(project_title=project_title_url, commit=commit)

    @staticmethod
    @dispatch(ObjectId)
    def read_coverage(
            coverage_id: ObjectId) -> CoverageDatabaseType:
        return read_coverage(coverage_id=coverage_id)[0]

    @staticmethod
    @dispatch(str)
    def read_coverage(
            job_reference: str) -> CoverageDatabaseType:
        return read_coverage(job_reference=job_reference)[0]

    @staticmethod
    @dispatch(str, ObjectId)
    def read_coverage(
            description: str, commit_id: ObjectId) -> CoverageDatabaseType:
        return read_coverage(description=description, commit_id=commit_id)[0]

    @staticmethod
    @dispatch(str, str, ObjectId)
    def read_coverage(
            description: str, commit_hash: str, project_id: ObjectId) -> CoverageDatabaseType:
        return read_coverage(description=description, commit_hash=commit_hash, project_id=project_id)[0]

    @staticmethod
    @dispatch(str, str, str)
    def read_coverage(
            description: str, commit_hash: str, project_title_url: str) -> CoverageDatabaseType:
        if _is_url(project_title_url):
            return read_coverage(description=description, commit_hash=commit_hash,
                                 project_url=project_title_url)[0]
        else:
            return read_coverage(description=description, commit_hash=commit_hash,
                                 project_title=project_title_url)[0]

    @staticmethod
    @dispatch(CoverageDatabaseType, ObjectId)
    def write_coverage(
            coverage: CoverageDatabaseType, commit_id: ObjectId) -> ObjectId:
        return write_coverage(coverage=coverage, commit_id=commit_id)

    @staticmethod
    @dispatch(CoverageDatabaseType, str, ObjectId)
    def write_coverage(
            coverage: CoverageDatabaseType, commit_hash: str, project_id: ObjectId) -> ObjectId:
        return write_coverage(coverage=coverage, commit_hash=commit_hash, project_id=project_id)

    @staticmethod
    @dispatch(CoverageDatabaseType, str, str)
    def write_coverage(
            coverage: CoverageDatabaseType, commit_hash: str, project_title_url: str) -> ObjectId:
        if _is_url(project_title_url):
            return write_coverage(coverage=coverage, commit_hash=commit_hash, project_url=project_title_url)
        else:
            return write_coverage(coverage=coverage, commit_hash=commit_hash, project_title=project_title_url)


def _is_url(url_or_title: str) -> bool:
    if url_or_title.find("http") != -1:
        return True
    else:
        return False


def read_project(project_url: str = None, project_title: str = None, project_id: ObjectId = None,
                 get_all: bool = False) -> list[ProjectDatabaseType]:
    try:
        if get_all:
            return ProjectDatabaseType.objects
        elif project_id:
            project_database_result = ProjectDatabaseType.objects.get(id=project_id)
        elif project_url:
            project_database_result = ProjectDatabaseType.objects.get(url=project_url)
        elif project_title:
            project_database_result = ProjectDatabaseType.objects.get(title=project_title)
        else:
            raise MissingInputParameters("missing arguments read_project")
        return [project_database_result]
    except DoesNotExist as exc:
        raise NotFound("project") from exc


def write_project(project: ProjectDatabaseType = None) -> ObjectId:
    try:
        project.save()
        return project.id
    except NotUniqueError as exc:
        raise NotUnique("project") from exc


def read_commit(project_url: str = None, project_title: str = None, project_id: ObjectId = None,
                commit_hash: str = None, commit_id: ObjectId = None, get_all: bool = False) -> list[CommitDatabaseType]:
    if commit_id:
        try:
            commit_database_result = CommitDatabaseType.objects.get(id=commit_id)
        except DoesNotExist as exc:
            raise NotFound("commit") from exc
    elif (get_all or commit_hash) and (project_id or project_url or project_title):
        try:
            if project_id:
                project_database_result = ProjectDatabaseType.objects.get(id=project_id)
            elif project_url:
                project_database_result = ProjectDatabaseType.objects.get(url=project_url)
            elif project_title:
                project_database_result = ProjectDatabaseType.objects.get(title=project_title)
            else:
                raise MissingInputParameters("missing arguments read_commit")
        except DoesNotExist as exc:
            raise NotFound("project") from exc
        if get_all:
            return CommitDatabaseType.objects(project=project_database_result)
        try:
            commit_database_result = CommitDatabaseType.objects.get(commit_hash=commit_hash,
                                                                    project=project_database_result)
        except DoesNotExist as exc:
            raise NotFound("commit") from exc
    else:
        raise MissingInputParameters("missing arguments read_commit")
    return [commit_database_result]


def write_commit(project_url: str = None, project_title: str = None, project_id: ObjectId = None,
                 commit: CommitDatabaseType = None) -> ObjectId:
    try:
        if project_id:
            project_database_result = ProjectDatabaseType.objects.get(id=project_id)
        elif project_url:
            project_database_result = ProjectDatabaseType.objects.get(url=project_url)
        elif project_title:
            project_database_result = ProjectDatabaseType.objects.get(title=project_title)
        else:
            raise MissingInputParameters("missing arguments write_commit")
    except DoesNotExist as exc:
        raise NotFound("project") from exc
    try:
        commit.project = project_database_result
        commit.save()
        return commit.id
    except NotUniqueError as exc:
        raise NotUnique("commit") from exc


def read_coverage(project_url: str = None, project_title: str = None, project_id: ObjectId = None,
                  commit_hash: str = None, commit_id: ObjectId = None, get_all: bool = False,
                  description: str = None, job_reference: str = None, coverage_id: ObjectId = None
                  ) -> list[CoverageDatabaseType]:
    if coverage_id:
        try:
            coverage_database_results = CoverageDatabaseType.objects.get(id=coverage_id)
        except DoesNotExist as exc:
            raise NotFound("coverage") from exc
    elif job_reference:
        try:
            coverage_database_results = CoverageDatabaseType.objects.get(job_reference=job_reference)
        except DoesNotExist as exc:
            raise NotFound("coverage") from exc
    elif (get_all or description) and commit_id:
        try:
            commit_database_result = CommitDatabaseType.objects.get(id=commit_id)
        except DoesNotExist as exc:
            raise NotFound("commit") from exc
        if get_all:
            return CoverageDatabaseType.objects(commit=commit_database_result)
        try:
            coverage_database_results = CoverageDatabaseType.objects.get(commit=commit_database_result,
                                                                         description=description)
        except DoesNotExist as exc:
            raise NotFound("coverage") from exc
    elif (get_all or description) and commit_hash and (project_id or project_url or project_title):
        try:
            if project_id:
                project_database_result = ProjectDatabaseType.objects.get(id=project_id)
            elif project_url:
                project_database_result = ProjectDatabaseType.objects.get(url=project_url)
            elif project_title:
                project_database_result = ProjectDatabaseType.objects.get(title=project_title)
            else:
                raise MissingInputParameters("missing arguments read_coverage")
        except DoesNotExist as exc:
            raise NotFound("project") from exc
        try:
            commit_database_result = CommitDatabaseType.objects.get(project=project_database_result,
                                                                    commit_hash=commit_hash)
        except DoesNotExist as exc:
            raise NotFound("commit") from exc
        if get_all:
            return CoverageDatabaseType.objects(commit=commit_database_result)
        try:
            coverage_database_results = CoverageDatabaseType.objects.get(commit=commit_database_result,
                                                                         description=description)
        except DoesNotExist as exc:
            raise NotFound("coverage") from exc
    else:
        raise MissingInputParameters("missing arguments read_coverage")
    if coverage_database_results:
        return [coverage_database_results]
    raise NotFound(f"Coverage not found! project_url: {project_url}, project_title: {project_title} "
                   f",commit_hash: {commit_hash}, coverage_id: {coverage_id}, description: {description}")


def write_coverage(coverage: CoverageDatabaseType,
                   project_url: str = None, project_title: str = None, project_id: ObjectId = None,
                   commit_hash: str = None, commit_id: ObjectId = None) -> ObjectId:
    if commit_id:
        try:
            commit_database_result = CommitDatabaseType.objects.get(id=commit_id)
        except DoesNotExist as exc:
            raise NotFound("commit") from exc
    elif commit_hash and (project_id or project_url or project_title):
        try:
            if project_id:
                project_database_result = ProjectDatabaseType.objects.get(id=project_id)
            elif project_url:
                project_database_result = ProjectDatabaseType.objects.get(url=project_url)
            elif project_title:
                project_database_result = ProjectDatabaseType.objects.get(title=project_title)
            else:
                raise MissingInputParameters("missing arguments write_coverage")
        except DoesNotExist as exc:
            raise NotFound("project") from exc
        try:
            commit_database_result = CommitDatabaseType.objects.get(project=project_database_result,
                                                                    commit_hash=commit_hash)
        except DoesNotExist as exc:
            raise NotFound("commit") from exc
    else:
        raise MissingInputParameters("missing arguments in write_coverage")
    try:
        coverage.commit = commit_database_result
        coverage.save()
        return coverage.id
    except NotUniqueError as exc:
        raise NotUnique("coverage") from exc


# TODO merge swagger and database types instead
def convert_to_api_type(data: [list[ProjectDatabaseType], list[CoverageDatabaseType], list[CoverageDatabaseType]]
                        ) -> [list[ProjectType], list[CommitType], list[CoverageType]]:
    data_type = type(data)
    if data_type is list or data_type is QuerySet:
        data_type = type(data[0])
    else:
        raise MissingInputParameters("wrong type for conversion must be of type list[<DatabaseType>]")
    if data_type is ProjectDatabaseType:
        return [
            ProjectType(
                url=project.url,
                title=project.title,
                description=project.description,
            ) for project in data
        ]
    elif data_type is CommitDatabaseType:
        return [
            CommitType(
                commit_hash=commit.commit_hash,
                message=commit.message,
                branch=commit.branch,
            ) for commit in data
        ]
    elif data_type is CoverageDatabaseType:
        return [CoverageType(
            job_reference=coverage.job_reference, description=coverage.description,
            doctype=coverage.doctype, encoding=coverage.encoding,
            version=coverage.version, timestamp=coverage.timestamp,
            lines_valid=coverage.lines_valid, lines_covered=coverage.lines_covered,
            line_rate=coverage.line_rate, branches_covered=coverage.branches_covered,
            branches_valid=coverage.branches_valid, branch_rate=coverage.branch_rate,
            complexity=coverage.complexity,
            sources=_create_source_instances(coverage.sources),
            packages=_create_package_instances(coverage.packages),
        ) for coverage in data]
    else:
        raise MissingInputParameters("wrong type for conversion")


# TODO merge swagger and database types instead
def convert_from_api_type(data: [ProjectType, CommitType, CoverageType]
                          ) -> [ProjectDatabaseType, CoverageDatabaseType, CoverageDatabaseType]:
    data_type = type(data)
    if data_type is ProjectType:
        return ProjectDatabaseType(
            url=data.url,
            title=data.title,
            description=data.description,
        )

    elif data_type is CommitType:
        return CommitDatabaseType(
            commit_hash=data.commit_hash,
            message=data.message,
            branch=data.branch,
            project=None,  # NOTE must be set
        )
    elif data_type is CoverageType:
        return CoverageDatabaseType(
            job_reference=data.job_reference, description=data.description,
            doctype=data.doctype, encoding=data.encoding,
            version=data.version, timestamp=data.timestamp,
            lines_valid=data.lines_valid, lines_covered=data.lines_covered,
            line_rate=data.line_rate, branches_covered=data.branches_covered,
            branches_valid=data.branches_valid, branch_rate=data.branch_rate,
            complexity=data.complexity,
            sources=_create_source_instances(data.sources),
            packages=_create_package_instances(data.packages),
            commit=None,  # NOTE must be set
        )
    else:
        raise MissingInputParameters("wrong type for conversion")


def _create_class_instances(classes: [list[ClassType], list[ClassDatabaseType]]
                            ) -> [list[MethodeType], list[MethodeDatabaseType]]:
    class_instances = []
    for _class in classes:
        method_instances = []
        for method in _class.methods:
            if type(method) == MethodeDatabaseType:
                method_instances.append(
                    MethodeType(
                        name=method.name,
                        filename=method.filename,
                        complexity=method.complexity,
                        line_rate=method.line_rate,
                        branch_rate=method.branch_rate,
                        lines=[LineType.from_dict(line) for line in method.lines],
                    )
                )
            else:
                method_instances.append(
                    MethodeDatabaseType(
                        name=method.name,
                        filename=method.filename,
                        complexity=method.complexity,
                        line_rate=method.line_rate,
                        branch_rate=method.branch_rate,
                        lines=[line.to_dict() for line in method.lines],
                    )
                )
        if type(_class) == ClassDatabaseType:
            class_instances.append(
                ClassType(
                    name=_class.name,
                    filename=_class.filename,
                    complexity=_class.complexity,
                    line_rate=_class.line_rate,
                    branch_rate=_class.branch_rate,
                    methods=method_instances,
                    lines=[LineType.from_dict(line) for line in _class.lines],
                )
            )
        else:
            class_instances.append(
                ClassDatabaseType(
                    name=_class.name,
                    filename=_class.filename,
                    complexity=_class.complexity,
                    line_rate=_class.line_rate,
                    branch_rate=_class.branch_rate,
                    methods=method_instances,
                    lines=[line.to_dict() for line in _class.lines],
                )
            )
    return class_instances


def _create_source_instances(sources: [list[SourceType], list[SourceDatabaseType]]
                             ) -> [list[SourceType], list[SourceDatabaseType]]:
    source_instances = []
    for source in sources:
        if type(source) == SourceDatabaseType:
            source_instances.append(
                SourceType(path=source.path)
            )
        else:
            source_instances.append(
                SourceDatabaseType(path=source.path)
            )
    return source_instances


def _create_package_instances(packages: [list[PackageType], list[SourceDatabaseType]]
                              ) -> [list[PackageType], list[PackageDatabaseType]]:
    package_instances = []
    for package in packages:
        if type(package) == PackageDatabaseType:
            package_instances.append(
                PackageType(
                    name=package.name,
                    line_rate=package.line_rate,
                    branch_rate=package.branch_rate,
                    complexity=package.complexity,
                    classes=_create_class_instances(package.classes),
                )
            )
        else:
            package_instances.append(
                PackageDatabaseType(
                    name=package.name,
                    line_rate=package.line_rate,
                    branch_rate=package.branch_rate,
                    complexity=package.complexity,
                    classes=_create_class_instances(package.classes),
                )
            )
    return package_instances
