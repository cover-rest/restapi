import mongoengine

# TODO maybe move this or do check for connection
db = mongoengine.connect(host="mongodb://127.0.0.1:27017/cover_rest_db")

from .types.coverage_type import CoverageDatabaseType
from .types.commit_type import CommitDatabaseType
from .types.project_type import ProjectDatabaseType

from .database_interface import DatabaseInterface
from .database_interface import convert_from_api_type
from .database_interface import convert_to_api_type
