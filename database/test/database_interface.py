from unittest import main, TestCase

import mongoengine

from bson.objectid import ObjectId

from .. import database_interface
from ...parser.xml_parse import XmlParse
from ..types.project_type import ProjectDatabaseType
from ..types.commit_type import CommitDatabaseType
from ...swagger_server.models.coverage_type import CoverageType

mongoengine.disconnect()
db = mongoengine.connect(host="mongodb://127.0.0.1:27017/cover_rest_test_db")
db.drop_database("cover_rest_test_db")

project_url = "https://TestProject.com"
project_title = "TestProject"
project_description = "This is a Test Project"

commit_hash = "test1234"
commit_message = ""
commit_branch = ""

coverage_path = "coverage_python.xml"
coverage_job_reference = "testCoverage"


def get_test_project():
    test_project = ProjectDatabaseType(
        url=project_url,
        title=project_title,
        description=project_description,
    )
    return test_project


def get_test_commit():
    test_commit = CommitDatabaseType(
        commit_hash=commit_hash,
        message=commit_message,
        branch=commit_branch
    )
    return test_commit


def get_test_coverage():  # TODO find cleaner solution
    dataType = XmlParse.from_path(coverage_path)
    dataType.coverage.job_reference = coverage_job_reference
    test_coverage = database_interface.convert_from_api_type(CoverageType.from_dict(dataType.to_dict()))
    return test_coverage


class TestProject(TestCase):
    def test_write_project(self):
        db = mongoengine.get_connection()
        db.drop_database("cover_rest_test_db")
        test_project = get_test_project()
        project_id = database_interface.DatabaseInterface.write_project(test_project)
        self.assertIsInstance(project_id, ObjectId)

    def test_read_project_id(self):
        db = mongoengine.get_connection()
        db.drop_database("cover_rest_test_db")
        test_project = get_test_project()
        project_id = database_interface.DatabaseInterface.write_project(test_project)
        self.assertIsInstance(project_id, ObjectId)
        project = database_interface.DatabaseInterface.read_project(project_id)
        self.assertEqual(
            project,
            test_project)

    def test_read_project_url(self):
        db = mongoengine.get_connection()
        db.drop_database("cover_rest_test_db")
        test_project = get_test_project()
        project_id = database_interface.DatabaseInterface.write_project(test_project)
        self.assertIsInstance(project_id, ObjectId)
        project = database_interface.DatabaseInterface.read_project(project_url)
        self.assertEqual(
            project,
            test_project)

    def test_read_project_title(self):
        db = mongoengine.get_connection()
        db.drop_database("cover_rest_test_db")
        test_project = get_test_project()
        project_id = database_interface.DatabaseInterface.write_project(test_project)
        self.assertIsInstance(project_id, ObjectId)
        project = database_interface.DatabaseInterface.read_project(project_title)
        self.assertEqual(
            project,
            test_project)


class TestCommit(TestCase):
    def test_write_commit_project_id(self):
        db = mongoengine.get_connection()
        db.drop_database("cover_rest_test_db")
        test_project = get_test_project()
        test_commit = get_test_commit()
        project_id = database_interface.DatabaseInterface.write_project(test_project)
        self.assertIsInstance(project_id, ObjectId)
        commit_id = database_interface.DatabaseInterface.write_commit(project_id, test_commit)
        self.assertIsInstance(commit_id, ObjectId)

    def test_write_commit_project_url(self):
        db = mongoengine.get_connection()
        db.drop_database("cover_rest_test_db")
        test_project = get_test_project()
        test_commit = get_test_commit()
        project_id = database_interface.DatabaseInterface.write_project(test_project)
        self.assertIsInstance(project_id, ObjectId)
        commit_id = database_interface.DatabaseInterface.write_commit(project_url, test_commit)
        self.assertIsInstance(commit_id, ObjectId)

    def test_write_commit_project_title(self):
        db = mongoengine.get_connection()
        db.drop_database("cover_rest_test_db")
        test_project = get_test_project()
        test_commit = get_test_commit()
        project_id = database_interface.DatabaseInterface.write_project(test_project)
        self.assertIsInstance(project_id, ObjectId)
        commit_id = database_interface.DatabaseInterface.write_commit(project_title, test_commit)
        self.assertIsInstance(commit_id, ObjectId)

    def test_read_commit_id(self):
        db = mongoengine.get_connection()
        db.drop_database("cover_rest_test_db")
        test_project = get_test_project()
        test_commit = get_test_commit()
        project_id = database_interface.DatabaseInterface.write_project(test_project)
        self.assertIsInstance(project_id, ObjectId)
        commit_id = database_interface.DatabaseInterface.write_commit(project_id, test_commit)
        self.assertIsInstance(commit_id, ObjectId)
        commit = database_interface.DatabaseInterface.read_commit(commit_id)
        self.assertEqual(
            commit,
            test_commit)

    def test_read_commit_hash_project_id(self):
        db = mongoengine.get_connection()
        db.drop_database("cover_rest_test_db")
        test_project = get_test_project()
        test_commit = get_test_commit()
        project_id = database_interface.DatabaseInterface.write_project(test_project)
        self.assertIsInstance(project_id, ObjectId)
        commit_id = database_interface.DatabaseInterface.write_commit(project_id, test_commit)
        self.assertIsInstance(commit_id, ObjectId)
        commit = database_interface.DatabaseInterface.read_commit(commit_hash, project_id)
        self.assertEqual(
            commit,
            test_commit)

    def test_read_commit_hash_project_url(self):
        db = mongoengine.get_connection()
        db.drop_database("cover_rest_test_db")
        test_project = get_test_project()
        test_commit = get_test_commit()
        project_id = database_interface.DatabaseInterface.write_project(test_project)
        self.assertIsInstance(project_id, ObjectId)
        commit_id = database_interface.DatabaseInterface.write_commit(project_id, test_commit)
        self.assertIsInstance(commit_id, ObjectId)
        commit = database_interface.DatabaseInterface.read_commit(commit_hash, project_url)
        self.assertEqual(
            commit,
            test_commit)

    def test_read_commit_hash_project_title(self):
        db = mongoengine.get_connection()
        db.drop_database("cover_rest_test_db")
        test_project = get_test_project()
        test_commit = get_test_commit()
        project_id = database_interface.DatabaseInterface.write_project(test_project)
        self.assertIsInstance(project_id, ObjectId)
        commit_id = database_interface.DatabaseInterface.write_commit(project_id, test_commit)
        self.assertIsInstance(commit_id, ObjectId)
        commit = database_interface.DatabaseInterface.read_commit(commit_hash, project_title)
        self.assertEqual(
            commit,
            test_commit)


class TestCoverage(TestCase):
    def test_write_coverage_commit_id(self):
        db = mongoengine.get_connection()
        db.drop_database("cover_rest_test_db")
        test_project = get_test_project()
        test_commit = get_test_commit()
        test_coverage = get_test_coverage()
        project_id = database_interface.DatabaseInterface.write_project(test_project)
        self.assertIsInstance(project_id, ObjectId)
        commit_id = database_interface.DatabaseInterface.write_commit(project_id, test_commit)
        self.assertIsInstance(commit_id, ObjectId)
        coverage_id = database_interface.DatabaseInterface.write_coverage(test_coverage, commit_id)
        self.assertIsInstance(coverage_id, ObjectId)

    def test_write_coverage_commit_hash_project_id(self):
        db = mongoengine.get_connection()
        db.drop_database("cover_rest_test_db")
        test_project = get_test_project()
        test_commit = get_test_commit()
        test_coverage = get_test_coverage()
        project_id = database_interface.DatabaseInterface.write_project(test_project)
        self.assertIsInstance(project_id, ObjectId)
        commit_id = database_interface.DatabaseInterface.write_commit(project_id, test_commit)
        self.assertIsInstance(commit_id, ObjectId)
        coverage_id = database_interface.DatabaseInterface.write_coverage(test_coverage, commit_hash, project_id)
        self.assertIsInstance(coverage_id, ObjectId)

    def test_write_coverage_commit_hash_project_url(self):
        db = mongoengine.get_connection()
        db.drop_database("cover_rest_test_db")
        test_project = get_test_project()
        test_commit = get_test_commit()
        test_coverage = get_test_coverage()
        project_id = database_interface.DatabaseInterface.write_project(test_project)
        self.assertIsInstance(project_id, ObjectId)
        commit_id = database_interface.DatabaseInterface.write_commit(project_id, test_commit)
        self.assertIsInstance(commit_id, ObjectId)
        coverage_id = database_interface.DatabaseInterface.write_coverage(test_coverage, commit_hash, project_url)
        self.assertIsInstance(coverage_id, ObjectId)

    def test_write_coverage_commit_hash_project_title(self):
        db = mongoengine.get_connection()
        db.drop_database("cover_rest_test_db")
        test_project = get_test_project()
        test_commit = get_test_commit()
        test_coverage = get_test_coverage()
        project_id = database_interface.DatabaseInterface.write_project(test_project)
        self.assertIsInstance(project_id, ObjectId)
        commit_id = database_interface.DatabaseInterface.write_commit(project_id, test_commit)
        self.assertIsInstance(commit_id, ObjectId)
        coverage_id = database_interface.DatabaseInterface.write_coverage(test_coverage, commit_hash, project_title)
        self.assertIsInstance(coverage_id, ObjectId)

    def test_read_coverage_id(self):
        db = mongoengine.get_connection()
        db.drop_database("cover_rest_test_db")
        test_project = get_test_project()
        test_commit = get_test_commit()
        test_coverage = get_test_coverage()
        project_id = database_interface.DatabaseInterface.write_project(test_project)
        self.assertIsInstance(project_id, ObjectId)
        commit_id = database_interface.DatabaseInterface.write_commit(project_id, test_commit)
        self.assertIsInstance(commit_id, ObjectId)
        coverage_id = database_interface.DatabaseInterface.write_coverage(test_coverage, commit_hash, project_title)
        self.assertIsInstance(coverage_id, ObjectId)
        coverage = database_interface.DatabaseInterface.read_coverage(coverage_id)
        self.assertEqual(coverage, test_coverage)

    def test_read_coverage_ref_name_commit_id(self):
        db = mongoengine.get_connection()
        db.drop_database("cover_rest_test_db")
        test_project = get_test_project()
        test_commit = get_test_commit()
        test_coverage = get_test_coverage()
        project_id = database_interface.DatabaseInterface.write_project(test_project)
        self.assertIsInstance(project_id, ObjectId)
        commit_id = database_interface.DatabaseInterface.write_commit(project_id, test_commit)
        self.assertIsInstance(commit_id, ObjectId)
        coverage_id = database_interface.DatabaseInterface.write_coverage(test_coverage, commit_hash, project_title)
        self.assertIsInstance(coverage_id, ObjectId)
        coverage = database_interface.DatabaseInterface.read_coverage(coverage_job_reference)
        self.assertEqual(coverage, test_coverage)

    def test_read_coverage_ref_name_commit_hash_project_id(self):
        db = mongoengine.get_connection()
        db.drop_database("cover_rest_test_db")
        test_project = get_test_project()
        test_commit = get_test_commit()
        test_coverage = get_test_coverage()
        project_id = database_interface.DatabaseInterface.write_project(test_project)
        self.assertIsInstance(project_id, ObjectId)
        commit_id = database_interface.DatabaseInterface.write_commit(project_id, test_commit)
        self.assertIsInstance(commit_id, ObjectId)
        coverage_id = database_interface.DatabaseInterface.write_coverage(test_coverage, commit_hash, project_title)
        self.assertIsInstance(coverage_id, ObjectId)
        coverage = database_interface.DatabaseInterface.read_coverage(coverage_job_reference)
        self.assertEqual(coverage, test_coverage)

    def test_read_coverage_ref_name_commit_hash_project_url(self):
        db = mongoengine.get_connection()
        db.drop_database("cover_rest_test_db")
        test_project = get_test_project()
        test_commit = get_test_commit()
        test_coverage = get_test_coverage()
        project_id = database_interface.DatabaseInterface.write_project(test_project)
        self.assertIsInstance(project_id, ObjectId)
        commit_id = database_interface.DatabaseInterface.write_commit(project_id, test_commit)
        self.assertIsInstance(commit_id, ObjectId)
        coverage_id = database_interface.DatabaseInterface.write_coverage(test_coverage, commit_hash, project_title)
        self.assertIsInstance(coverage_id, ObjectId)
        coverage = database_interface.DatabaseInterface.read_coverage(coverage_job_reference)
        self.assertEqual(coverage, test_coverage)

    def test_read_coverage_ref_name_commit_hash_project_title(self):
        db = mongoengine.get_connection()
        db.drop_database("cover_rest_test_db")
        test_project = get_test_project()
        test_commit = get_test_commit()
        test_coverage = get_test_coverage()
        project_id = database_interface.DatabaseInterface.write_project(test_project)
        self.assertIsInstance(project_id, ObjectId)
        commit_id = database_interface.DatabaseInterface.write_commit(project_id, test_commit)
        self.assertIsInstance(commit_id, ObjectId)
        coverage_id = database_interface.DatabaseInterface.write_coverage(test_coverage, commit_hash, project_title)
        self.assertIsInstance(coverage_id, ObjectId)
        coverage = database_interface.DatabaseInterface.read_coverage(coverage_job_reference)
        self.assertEqual(coverage, test_coverage)


if __name__ == '__main__':
    main()
