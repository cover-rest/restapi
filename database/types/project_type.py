from mongoengine import Document, StringField


class ProjectDatabaseType(Document):
    url = StringField(unique=True, required=True)
    title = StringField(unique=True, required=True)
    description = StringField()

    meta = {
        "indexes": ["url", "title"]
    }
