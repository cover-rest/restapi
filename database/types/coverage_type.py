from mongoengine import DynamicDocument
from mongoengine import IntField, StringField, FloatField, ListField, DictField
from mongoengine import ReferenceField, CASCADE


class MethodeDatabaseType(DynamicDocument):
    name = StringField(required=True)
    filename = StringField(required=True)
    complexity = FloatField(required=True)
    line_rate = FloatField(required=True)
    branch_rate = FloatField(required=True)
    lines = ListField(DictField())

    meta = {
        "indexes": ["filename"],
        "cascade": True,
    }


class ClassDatabaseType(DynamicDocument):
    name = StringField(required=True)
    filename = StringField(required=True)
    complexity = FloatField(required=True)
    line_rate = FloatField(required=True)
    branch_rate = FloatField(required=True)
    methods = ListField(ReferenceField(MethodeDatabaseType, reverse_delete_rule=CASCADE))
    lines = ListField(DictField())

    meta = {
        "indexes": ["filename"],
        "cascade": True,
    }


class PackageDatabaseType(DynamicDocument):
    name = StringField(required=True)
    line_rate = FloatField(required=True)
    branch_rate = FloatField(required=True)
    complexity = FloatField(required=True)
    classes = ListField(ReferenceField(ClassDatabaseType, reverse_delete_rule=CASCADE))

    meta = {
        "indexes": ["name"],
        "cascade": True,
    }


class SourceDatabaseType(DynamicDocument):
    path = StringField(required=True)

    meta = {
        "cascade": True,
    }


class CoverageDatabaseType(DynamicDocument):
    commit = ReferenceField("CommitDatabaseType")
    job_reference = StringField(required=True, unique=True)
    description = StringField()
    doctype = StringField(default="<!DOCTYPE coverage SYSTEM 'http://cobertura.sourceforge.net/xml/coverage-04.dtd'>")
    encoding = StringField(default="UTF-8")
    version = StringField(default="")
    timestamp = StringField(default="")
    lines_valid = IntField(required=True)
    lines_covered = IntField(required=True)
    line_rate = FloatField(required=True)
    branches_covered = IntField(required=True)
    branches_valid = IntField(required=True)
    branch_rate = FloatField(required=True)
    complexity = FloatField(required=True)
    sources = ListField(ReferenceField(SourceDatabaseType, reverse_delete_rule=CASCADE))
    packages = ListField(ReferenceField(PackageDatabaseType, reverse_delete_rule=CASCADE))

    meta = {
        "cascade": True,
        "indexes": ["job_reference", "reference_name"],
    }
