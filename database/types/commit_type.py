from mongoengine import Document, StringField
from mongoengine import ReferenceField


class CommitDatabaseType(Document):
    commit_hash = StringField(unique=True, required=True)
    message = StringField()
    branch = StringField(required=True)
    project = ReferenceField("ProjectDatabaseType")

    meta = {
        "indexes": ["commit_hash"]
    }
