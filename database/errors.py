

class DatabaseBaseException(Exception):
    pass


class NotFound(DatabaseBaseException):
    pass


class NotUnique(DatabaseBaseException):
    pass


class MissingInputParameters(DatabaseBaseException):
    pass


class NotAuthorized(DatabaseBaseException):
    pass
