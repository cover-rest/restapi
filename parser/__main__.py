import os
import argparse
import time
import logging
import requests

from .xml_parse import XmlParse


if __name__ == "__main__":
    root_logger = logging.getLogger()
    root_logger.setLevel(logging.INFO)

    parser = argparse.ArgumentParser(description='Parse Xml to Python Object -> send and receive them over the API')
    parser.add_argument("file",
                        help="input xml report file")
    parser.add_argument("-o", "--output-file", action="store", nargs="?", const=True, default=False,
                        help="output xml file from parse")
    parser.add_argument("--pull-from-api", default=False,
                        metavar="API_URL", help="from API to xml file, have to supply url")
    parser.add_argument("--push-to-api", action="store", nargs="?", default=False,
                        metavar="API_URL", help="push xml file to API, have to supply url")
    parser.add_argument("-r", "--job-reference", default=False,
                        help="required for referencing coverage in database")
    parser.add_argument("--description", default=False,
                        help="description for coverage in database")
    parser.add_argument("-d", "--debug", action="store_true", default=False,
                        help="enable debug logging")
    parser.add_argument("-s", "--silent", action="store_true", default=False,
                        help="only show warnings and critical logging")
    parser.add_argument("--no-reduce", action="store_true", default=False,
                        help="if True lines without hits wont be parsed")
    parser.add_argument("--not-sequential", action="store_true", default=False,
                        help="if the not sequential, parser is used which might cause memory problems")
    parser.add_argument("--test-performance", action="store_true", default=False,
                        help="performance test loop, for debugging")
    parser.add_argument("-n", "--test-range", type=int, default=1,
                        metavar="TEST_AMOUNT", help="test_range for performance test, have to supply amount")

    args = parser.parse_args()

    logging.info(f"report for: {args.file}")

    if args.silent:
        root_logger.setLevel(logging.WARNING)

    elif args.debug:
        root_logger.setLevel(logging.DEBUG)

    if args.test_performance:
        times_from_python = []
        times_to_python = []

        logging.info("  file to python loop:")
        dataType = None
        for i in range(args.test_range):
            logging.info(f"    iter {i + 1} of {args.test_range}")
            start = time.perf_counter()
            dataType = XmlParse.from_path(args.file, not args.no_reduce)
            end = int((time.perf_counter() - start) * 1000)
            times_to_python.append(end)
            logging.info(f"    executing took: {end}ms")
            logging.debug(f"    {dataType.basic_report()}")

        if args.output_file:
            if type(args.output_file) == bool:
                basename = os.path.basename(args.file)
                coverage_type = ("cobertura" if dataType.root_element.doctype.find("cobertura") != -1 else "jacoco")
                args.output_file = os.path.join(
                    args.file.replace(basename, ""),  # filepath
                    f"{coverage_type}_{basename.replace('.xml', '')}{'' if not args.no_reduce else '_stripped'}.cov.xml"
                )
            logging.info(f"output file: {args.output_file}")
            logging.info("  python to file loop:")
            for i in range(args.test_range):
                logging.info(f"    iter {i + 1} of {args.test_range}")
                start = time.perf_counter()
                dataType.to_xml(args.output_file)
                end = int((time.perf_counter() - start) * 1000)
                times_from_python.append(end)
                logging.info(f"    executing took: {end}ms")

        logging.info("\n\n  file to python results:")
        avg_to_python = sum(times_to_python) / len(times_to_python)
        logging.info(f"    avg: {avg_to_python}")

        if args.output_file:
            logging.info("\n  python to file results:")
            avg_from_python = sum(times_from_python) / len(times_from_python)
            logging.info(f"    avg: {avg_from_python}")

    else:
        start_time = time.perf_counter()
        if args.pull_from_api:
            if args.job_reference and str(args.pull_from_api).find("jobReference") == -1:
                args.pull_from_api = f"{args.pull_from_api}?jobReference={args.job_reference}"
            dataType = XmlParse.from_api_db(args.file, args.pull_from_api, True)
        else:
            dataType = XmlParse.from_path(args.file, not args.no_reduce)
            logging.info(f"basic report: {dataType.basic_report()}")
        if args.push_to_api:
            if not args.job_reference:
                raise Exception("job_reference is required to push to api")
            response = dataType.to_api_db(args.push_to_api,
                                          job_reference=args.job_reference,
                                          description=args.description if args.description else "")
            logging.info(f"status code: {response.status_code}")
        if args.output_file:
            if type(args.output_file) == bool:
                basename = os.path.basename(args.file)
                coverage_type = ("cobertura" if dataType.coverage.doctype.find("cobertura") != -1 else "jacoco")
                args.output_file = os.path.join(
                    args.file.replace(basename, ""),  # filepath
                    f"{coverage_type}_{basename.replace('.xml', '')}{'' if not args.no_reduce else '_stripped'}.cov.xml"
                )
            logging.info(f"output file: {args.output_file}")
            dataType.to_xml(args.output_file)
        logging.debug(f"took: {time.perf_counter()-start_time}s")
