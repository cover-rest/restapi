from unittest import main, TestCase

from ..xml_parse import XmlParse


test_file_cobertura = "coverage_python.xml"
test_file_jacoco = "jacoco.xml"


class TestCobertura(TestCase):
    def test_from_path(self):
        xml_parse = XmlParse.from_path(test_file_cobertura)
        self.assertIsNotNone(xml_parse.coverage)

    def test_to_dict(self):
        xml_parse = XmlParse.from_path(test_file_cobertura)
        self.assertIsNotNone(xml_parse.coverage)
        xml_parse_dict = xml_parse.to_dict()
        self.assertIsInstance(xml_parse_dict, dict)

    def test_from_dict(self):
        xml_parse = XmlParse.from_path(test_file_cobertura)
        self.assertIsNotNone(xml_parse.coverage)
        xml_parse_dict = xml_parse.to_dict()
        self.assertIsInstance(xml_parse_dict, dict)
        xml_parse_from_dict = xml_parse.from_dict(xml_parse_dict)
        self.assertIsNotNone(xml_parse_from_dict.coverage)


class TestJacoco(TestCase):
    def test_xml_parse(self):
        xml_parse = XmlParse.from_path(test_file_jacoco)
        self.assertIsNotNone(xml_parse.coverage)


if __name__ == '__main__':
    main()
