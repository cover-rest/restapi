import logging
import os.path

import requests

from lxml import etree
from dataclasses import dataclass, asdict
from io import BytesIO

from .cover2cover import cover2cover


class XmlParse:
    coverage = None
    reduce: bool = True

    @classmethod
    def from_path(cls, path: str, reduce: bool = True):
        self = cls()
        self.reduce = reduce
        self.coverage = self.__parse_coverage(path)
        return self

    @classmethod
    def from_string(cls, input_string, reduce: bool = True):
        self = cls()
        self.reduce = reduce
        self.coverage = self.__parse_coverage(string=input_string)
        return self

    @classmethod
    def from_api_db(cls, _output_path, _url, _force_filename):
        self = cls()
        _res = requests.get(_url)
        logging.debug(f"status code: {_res.status_code}, response dict: {_res.__dict__}")
        status_code = _res.status_code
        if status_code != 200:
            raise Exception(_res.status_code, _res.content)
        else:
            if os.path.isdir(_output_path):
                base_path = _output_path
            else:
                base_path = "/".join(_output_path.split("/")[:-1])
            coverages = _res.json()
            for coverage in coverages:
                self.coverage = self.__parse_coverage_from_dict(coverage)
                logging.info(f"writing: {self.coverage.job_reference} coverage")
                if _force_filename and len(coverages) == 1:
                    self.to_xml(_output_path)
                else:
                    self.to_xml(os.path.join(base_path,
                                             f"cobertura_{self.coverage.job_reference}_stripped.cov.xml"))
        return self

    @classmethod
    def from_dict(cls, input_dict: dict):
        self = cls()
        self.coverage = self.__parse_coverage_from_dict(input_dict)
        return self

    @staticmethod
    def __get_doc_info(_path, string):
        flag_open = False
        xml_info = "<?"
        doc_info = "<!"
        doc_comment = "<!--"
        encoding = "UTF-8"
        version = "1.0"
        if _path:
            with open(_path, "r") as file:
                file_start = file.read(1000)
        else:
            file_start = string[:1000]
        for byte in file_start:
            if byte == "<":
                flag_open = True
            elif byte == ">":
                if flag_open == xml_info[1]:
                    xml_info += byte
                elif flag_open == doc_info[1]:
                    doc_info += byte
                flag_open = False
            elif flag_open is True and byte == xml_info[1]:
                flag_open = xml_info[1]
            elif flag_open is True and byte == doc_info[1]:
                flag_open = doc_info[1]
            elif flag_open == xml_info[1]:
                xml_info += byte
            elif flag_open == doc_info[1]:
                if doc_info == doc_comment:  # This is for skipping comments
                    doc_info = doc_info[:-2]
                    flag_open = False
                    continue
                doc_info += byte
        if doc_info == "<!":
            if file_start.find("counter") > 5:
                doc_info = '<!DOCTYPE report PUBLIC "-//JACOCO//DTD Report 1.1//EN" "report.dtd">'
            else:
                doc_info = "<!DOCTYPE coverage SYSTEM 'http://cobertura.sourceforge.net/xml/coverage-04.dtd'>"
        xml_info = xml_info.replace("?", "").replace("<", "").replace(">", "")
        for item in xml_info.split(" "):
            if item.find("encoding") != -1:
                encoding = item.split("=")[-1].replace("\'", "").replace("\"", "")
            elif item.find("version") != -1:
                version = item.split("=")[-1].replace("\'", "").replace("\"", "")
        return {"doctype": doc_info, "encoding": encoding, "xml_version": version}

    def __parse_coverage(self, _path=None, string=None):
        root_element = None
        last_line_parent = ""
        skip_line = False
        info = self.__get_doc_info(_path, string)
        if info.get("doctype").lower().find("jacoco") != -1:
            logging.warning(f"trying to parse jacoco! parsing to cobertura using cover2cover!")
            if not string:
                with open(_path, 'r') as file:
                    string = file.read().replace('\n', '')
            string = cover2cover.jacoco2cobertura(string=string)
        if string:
            iter_tree = etree.iterparse(BytesIO(string.encode("utf-8")), events=["start", "end"])
        elif _path:
            iter_tree = etree.iterparse(_path, events=["start", "end"])
        else:
            raise AttributeError("missing input")
        for event, element in iter_tree:
            if event == "start":
                dict_data = element.attrib
                if element.tag == "coverage":
                    root_element = RootType(
                        job_reference=dict_data.get("job_reference"),
                        description=dict_data.get("description"),
                        doctype=info.get("doctype"),  # REQUIRED
                        encoding=info.get("encoding"),  # REQUIRED
                        version=dict_data.get("version"),  # REQUIRED
                        timestamp=dict_data.get("timestamp"),  # REQUIRED
                        lines_valid=int(dict_data.get("lines-valid")),  # REQUIRED
                        lines_covered=int(dict_data.get("lines-covered")),  # REQUIRED
                        line_rate=float(dict_data.get("line-rate")),  # REQUIRED
                        branches_covered=int(dict_data.get("branches-covered")),  # REQUIRED
                        branches_valid=int(dict_data.get("branches-valid")),  # REQUIRED
                        branch_rate=float(dict_data.get("branch-rate")),  # REQUIRED
                        complexity=float(dict_data.get("complexity")),  # REQUIRED
                        sources=[],
                        packages=[],
                    )
                elif element.tag == "source":
                    root_element.sources.append(SourceType(path=element.text))
                elif element.tag == "package":
                    root_element.packages.append(PackageType(
                        name=dict_data.get("name"),
                        line_rate=float(dict_data.get("line-rate")),
                        branch_rate=float(dict_data.get("branch-rate")),
                        complexity=float(dict_data.get("complexity")),
                        classes=[],
                    ))
                elif element.tag == "class":
                    last_line_parent = element.tag
                    root_element.packages[-1].classes.append(
                        ClassType(
                            name=dict_data.get("name"),
                            filename=dict_data.get("filename"),
                            complexity=float(dict_data.get("complexity")),
                            line_rate=float(dict_data.get("line-rate")),
                            branch_rate=float(dict_data.get("branch-rate")),
                            methods=[],
                            lines=[],
                        )
                    )
                elif element.tag == "methods":
                    last_line_parent = "method"
                elif element.tag == "method":
                    root_element.packages[-1].classes[-1].methods.append(
                        MethodeType(
                            name=dict_data.get("name"),
                            filename=dict_data.get("filename"),
                            complexity=float(dict_data.get("complexity")),
                            line_rate=float(dict_data.get("line-rate")),
                            branch_rate=float(dict_data.get("branch-rate")),
                            lines=[],
                        )
                    )
                elif element.tag == "line":
                    branch = dict_data.get("branch")
                    hits = int(dict_data.get("hits"))
                    if hits == 0 and self.reduce:
                        skip_line = True
                        continue
                    if branch is not None and (str(branch).lower() == "true" or branch is True):
                        line = {
                            "number": str(dict_data.get("number")),
                            "hits": str(hits),
                            "branch": branch,
                            "condition_coverage": (dict_data.get("condition-coverage")),
                            "conditions": [],
                        }
                    else:
                        line = {
                            "number": str(dict_data.get("number")),
                            "hits": str(hits),
                        }
                    if last_line_parent == "class":
                        root_element.packages[-1].classes[-1].lines.append(
                            line
                        )
                    elif last_line_parent == "method":
                        root_element.packages[-1].classes[-1].methods[-1].lines.append(
                            line
                        )
                elif element.tag == "condition" and not skip_line:
                    condition = {
                        "number": str(dict_data.get("number")),
                        "type": dict_data.get("type"),
                        "coverage": dict_data.get("coverage"),
                    }
                    if last_line_parent == "class":
                        conditions = root_element.packages[-1].classes[-1].lines[-1].get("conditions")
                        conditions.append(condition)
                        root_element.packages[-1].classes[-1].lines[-1].update(
                            {"conditions": conditions}
                        )
                    elif last_line_parent == "method":
                        conditions = root_element.packages[-1].classes[-1].methods[-1].lines[-1].get("conditions")
                        conditions.append(condition)
                        root_element.packages[-1].classes[-1].methods[-1].lines[-1].update(
                            {"conditions": conditions}
                        )
            elif event == "end":
                if element.tag == "line" and skip_line:
                    skip_line = False
                elif element.tag == "method":
                    if len(root_element.packages[-1].classes[-1].methods[-1].lines) == 0:
                        root_element.packages[-1].classes[-1].methods.pop()
                elif element.tag == "class":
                    if (len(root_element.packages[-1].classes[-1].methods) == 0 and
                       len(root_element.packages[-1].classes[-1].lines) == 0):
                        root_element.packages[-1].classes.pop()
                elif element.tag == "methods":
                    last_line_parent = "class"
                elif element.tag == "package":
                    if len(root_element.packages[-1].classes) == 0:
                        root_element.packages.pop()
                elif element.tag == "coverage":
                    pass  # Done
                element.clear()
        return root_element

    def __parse_coverage_from_dict(self, dict_data: dict):
        root_element = RootType(
            job_reference=dict_data.get("job_reference"),
            description=dict_data.get("description"),
            doctype=dict_data.get("doctype"),  # REQUIRED
            encoding=dict_data.get("encoding"),  # REQUIRED
            version=dict_data.get("version"),  # REQUIRED
            timestamp=dict_data.get("timestamp"),  # REQUIRED
            lines_valid=int(dict_data.get("lines_valid")),  # REQUIRED
            lines_covered=int(dict_data.get("lines_covered")),  # REQUIRED
            line_rate=float(dict_data.get("line_rate")),  # REQUIRED
            branches_covered=int(dict_data.get("branches_covered")),  # REQUIRED
            branches_valid=int(dict_data.get("branches_valid")),  # REQUIRED
            branch_rate=float(dict_data.get("branch_rate")),  # REQUIRED
            complexity=float(dict_data.get("complexity")),  # REQUIRED
        )
        sources = []
        for source_dict in dict_data.get("sources"):
            sources.append(SourceType(path=source_dict.get("path")))
        packages = self.__parse_packages_from_dict(dict_data.get("packages"))
        root_element.sources = sources
        root_element.packages = packages
        return root_element

    def __parse_packages_from_dict(self, packages):
        package_elements = []
        for index, package in enumerate(packages):
            package_dict_data = package
            package_element = PackageType(
                name=package_dict_data.get("name"),
                line_rate=float(package_dict_data.get("line_rate")),
                branch_rate=float(package_dict_data.get("branch_rate")),
                complexity=float(package_dict_data.get("complexity")),
            )
            classes_elements = self.__parse_classes_from_dict(package_dict_data.get("classes"))
            if len(classes_elements) == 0:
                continue
            package_element.classes = classes_elements
            package_elements.append(package_element)
        return package_elements

    def __parse_classes_from_dict(self, classes):
        class_elements = []
        for _class in classes:
            class_dict_data = _class
            class_element = ClassType(
                name=class_dict_data.get("name"),
                filename=class_dict_data.get("filename"),
                complexity=float(class_dict_data.get("complexity")),
                line_rate=float(class_dict_data.get("line_rate")),
                branch_rate=float(class_dict_data.get("branch_rate")),
            )
            method_elements = self.__parse_methods_from_dict(class_dict_data.get("methods"))
            line_elements = self.__parse_lines_from_dict(class_dict_data.get("lines"))
            if (len(method_elements) + len(line_elements)) == 0:
                continue
            class_element.methods = method_elements
            class_element.lines = line_elements
            class_elements.append(class_element)
        return class_elements

    def __parse_methods_from_dict(self, methods):
        method_elements = []
        for method in methods:
            method_dict_data = method
            method_element = MethodeType(
                name=method_dict_data.get("name"),
                filename=method_dict_data.get("filename"),
                complexity=float(method_dict_data.get("complexity")),
                line_rate=float(method_dict_data.get("line_rate")),
                branch_rate=float(method_dict_data.get("branch_rate")),
            )
            line_elements = self.__parse_lines_from_dict(method_dict_data.get("lines"))
            if len(line_elements) == 0:
                continue
            method_element.lines = line_elements
            method_elements.append(method_element)
        return method_elements

    def __parse_lines_from_dict(self, lines):
        line_elements = []
        for line in lines:
            line_dict_data = line
            branch = line_dict_data.get("branch")
            hits = line_dict_data.get("hits")
            if hits == 0 and self.reduce:
                continue
            if branch is not None and (str(branch).lower() == "true" or branch is True):
                condition_elements = line_dict_data.get("conditions")
                line_elements.append({
                    "number": line_dict_data.get("number"),
                    "hits": hits,
                    "branch": branch,
                    "condition_coverage": line_dict_data.get("condition_coverage"),
                    "conditions": condition_elements,
                })
            else:
                line_elements.append({
                    "number": line_dict_data.get("number"),
                    "hits": hits,
                })
        return line_elements

    def to_api_db(self, _url, api_key, job_reference, description) -> requests.Response:
        self.coverage.job_reference = job_reference
        self.coverage.description = description
        _res = requests.post(_url, headers={'MODIFY-API-KEY': api_key}, json=self.to_dict())  # TODO impl api key
        logging.debug(f"status code: {_res.status_code}, response dict: {_res.__dict__}")
        status_code = _res.status_code
        if status_code != 201:
            raise Exception(_res.status_code, _res.content)
        return _res

    def to_dict(self) -> dict:
        return asdict(self.coverage)

    def to_xml(self, _filename) -> None:
        if not _filename.endswith('.xml'):
            raise Exception("filename must end with .xml")
        coverage = etree.Element("coverage", {
            "version": self.coverage.version,
            "timestamp": self.coverage.timestamp,
            "lines-valid": str(self.coverage.lines_valid),
            "lines-covered": str(self.coverage.lines_covered),
            "line-rate": str(self.coverage.line_rate),
            "branches-covered": str(self.coverage.branches_covered),
            "branches-valid": str(self.coverage.branches_valid),
            "branch-rate": str(self.coverage.branch_rate),
            "complexity": str(self.coverage.complexity),
        })

        sources = etree.SubElement(coverage, "sources")
        for source in self.coverage.sources:
            etree.SubElement(sources, "source").text = source.path

        packages = etree.SubElement(coverage, "packages")
        for package in self.coverage.packages:
            package_element = etree.SubElement(packages, "package", {
                "name": package.name,
                "line-rate": str(package.line_rate),
                "branch-rate": str(package.branch_rate),
                "complexity": str(package.complexity),
            })

            classes = etree.SubElement(package_element, "classes")
            for _class in package.classes:
                _class_element = etree.SubElement(classes, "class", {
                    "name": _class.name,
                    "filename": _class.filename,
                    "complexity": str(_class.complexity),
                    "line-rate": str(_class.line_rate),
                    "branch-rate": str(_class.branch_rate),
                })

                lines_class = etree.SubElement(_class_element, "lines")
                for line in _class.lines:
                    branch = line.get("branch")
                    if branch is None or str(branch).lower() == "false" or branch is False:
                        etree.SubElement(lines_class, "line", {
                            "number": str(line.get("number")),
                            "hits": str(line.get("hits")),
                        })
                    else:
                        line_element = etree.SubElement(lines_class, "line", {
                            "number": str(line.get("number")),
                            "hits": str(line.get("hits")),
                            "branch": line.get("branch"),
                            "condition-coverage": line.get("condition_coverage"),
                        })

                        conditions = etree.SubElement(line_element, "conditions")
                        for condition in line.get("conditions"):
                            etree.SubElement(conditions, "condition", {
                                "number": str(condition.get("number")),
                                "type": condition.get("type"),
                                "coverage": condition.get("coverage"),
                            })

                methods = etree.SubElement(_class_element, "methods")
                for methode in _class.methods:
                    methode_element = etree.SubElement(methods, "method", {
                        "name": methode.name,
                        "filename": methode.filename,
                        "complexity": str(methode.complexity),
                        "line-rate": str(methode.line_rate),
                        "branch-rate": str(methode.branch_rate)
                    })

                    lines_methods = etree.SubElement(methode_element, "lines")
                    for line in methode.lines:
                        branch = line.get("branch")
                        if branch is None or str(branch).lower() == "false" or branch is False:
                            etree.SubElement(lines_methods, "line", {
                                "number": str(line.get("number")),
                                "hits": str(line.get("hits")),
                            })
                        else:
                            line_element = etree.SubElement(lines_methods, "line", {
                                "number": str(line.get("number")),
                                "hits": str(line.get("hits")),
                                "branch": line.get("branch"),
                                "condition-coverage": line.get("condition_coverage"),
                            })

                            conditions = etree.SubElement(line_element, "conditions")
                            for condition in line.get("conditions"):
                                etree.SubElement(conditions, "condition", {
                                    "number": str(condition.get("number")),
                                    "type": condition.get("type"),
                                    "coverage": condition.get("coverage"),
                                })

        tree = etree.ElementTree(coverage)
        tree.write(_filename, encoding=self.coverage.encoding, xml_declaration=True,
                   doctype=self.coverage.doctype)

    def basic_report(self) -> dict:
        return {
            "line-rate": self.coverage.line_rate,
            "lines-valid": self.coverage.lines_valid,
            "lines-covered": self.coverage.lines_covered,
        }

    def simple_report(self) -> dict:
        return {
            "line-rate": self.coverage.line_rate,
            "lines-valid": self.coverage.lines_valid,
            "lines-covered": self.coverage.lines_covered,
            "timestamp": self.coverage.timestamp,
            "complexity": self.coverage.complexity,
            "packages": [{"path": package.path,
                          "line-rate": package.line_rate,
                          "classes": [{"path": _class.path,
                                       "line-rate": _class.line_rate,
                                       } for _class in package.classes]}
                         for package in self.coverage.packages]
        }


# to Set functions use empty init of function and then use class.__dict__ = dict to set the class vales
# missing how to set recursive class implementations


@dataclass(init=True)
class RootType:
    job_reference: str = ""
    description: str = ""
    doctype: str = ""
    encoding: str = ""
    version: str = ""
    timestamp: str = ""
    lines_valid: int = 0
    lines_covered: int = 0
    line_rate: float = 0.0
    branches_covered: int = 0
    branches_valid: int = 0
    branch_rate: float = 0.0
    complexity: float = 0.0
    sources: list = None
    packages: list = None


@dataclass(init=True)
class SourceType:
    path: str = ""


@dataclass(init=True)
class PackageType:
    name: str = ""
    line_rate: float = 0.0
    branch_rate: float = 0.0
    complexity: float = 0.0
    classes: list = None


@dataclass(init=True)
class ClassType:
    name: str = ""
    filename: str = ""
    complexity: float = 0.0
    line_rate: float = 0.0
    branch_rate: float = 0.0
    methods: list = None
    lines: list = None


@dataclass(init=True)
class MethodeType:
    name: str = ""
    filename: str = ""
    complexity: float = 0.0
    line_rate: float = 0.0
    branch_rate: float = 0.0
    lines: list = None
