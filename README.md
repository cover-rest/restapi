## Goal of rest-API
This is the Rest-API for the Cover-Rest Project.

This is the Glue between coverage files and tools, to Interface with the database.

## How to use API
push to api example: 
`python3 parser/xml_parse.py coverage_c.xml --push-to-api "http://localhost:8080/Cover-Rest/Interface-API/1.2.2/coverage?projectTitle=TestProject&commitHash=testcommit" -r "test-coverage-c"`

get from api example: 
`python3 parser/xml_parse.py coverage_c_from_api.xml --pull-from-api "http://localhost:8080/Cover-Rest/Interface-API/1.2.2/coverage" -r "test-coverage-c"`

API url: `http://localhost:8080/Cover-Rest/Interface-API/API_VERSION/`

## Database is mongodb:
To start mongodb after install: `sudo systemctl start mongod`

to install mongoengine use: `pip install git+https://github.com/nickfrev/mongoengine.git`

## Performance
- to get the best performance try using the newest python version
- cpython has same or even worse performance
- performance limit is IO and the time it takes python to create Objects
```bash
read of 350MB file takes about 7s
write of same file takes about 5s 
```

## Installation

if problems with submodules: `git submodule update --init --recursive`

## Tests
run coverage test: ` coverage run -m pytest database/test/* parsers/test/* swagger_server/test/* `
