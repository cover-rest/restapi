#!/usr/bin/env python3

import connexion
import argparse

from . import encoder


def main(port):
    app = connexion.App(__name__, specification_dir='./swagger/')
    app.app.json_encoder = encoder.JSONEncoder
    app.add_api('swagger.yaml', arguments={'title': 'Cover-Rest Interface API'}, pythonic_params=True)
    app.run(port=port)


if __name__ == '__main__':
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument("-p", "--port", help="port were the api runs", default=8080)
    args = arg_parser.parse_args()
    main(args.port)
