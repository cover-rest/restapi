import connexion
import six

from bson.objectid import ObjectId

from ..models.coverage_type import CoverageType  # noqa: E501
from ..models.object_id import ObjectID  # noqa: E501
from .. import util
from ...database import database_interface
from ...database.errors import NotFound, NotUnique, NotAuthorized, MissingInputParameters


def coverage_delete(project_url=None, project_title=None, project_id=None, commit_hash=None, commit_id=None, job_reference=None, description=None, coverage_id=None):  # noqa: E501
    """deletes Coverage

     # noqa: E501

    :param project_url: 
    :type project_url: str
    :param project_title: 
    :type project_title: str
    :param project_id: 
    :type project_id: str
    :param commit_hash: 
    :type commit_hash: str
    :param commit_id: 
    :type commit_id: str
    :param job_reference: 
    :type job_reference: str
    :param description: 
    :type description: str
    :param coverage_id: 
    :type coverage_id: str

    :rtype: ObjectID
    """
    return "not implemented", 501


def coverage_get(get_all=None, project_url=None, project_title=None, project_id=None, commit_hash=None, commit_id=None, job_reference=None, coverage_id=None):  # noqa: E501
    """returns Coverage

     # noqa: E501

    :param get_all:
    :type get_all: bool
    :param project_url: 
    :type project_url: str
    :param project_title: 
    :type project_title: str
    :param project_id: 
    :type project_id: str
    :param commit_hash: 
    :type commit_hash: str
    :param commit_id: 
    :type commit_id: str
    :param job_reference: 
    :type job_reference: str
    :param coverage_id: 
    :type coverage_id: str

    :rtype: List[CoverageType]
    """
    try:
        coverages = database_interface.read_coverage(
            project_url=project_url,
            project_title=project_title,
            project_id=ObjectId(project_id) if project_id else None,
            commit_hash=commit_hash,
            commit_id=ObjectId(commit_id) if commit_id else None,
            job_reference=job_reference,
            coverage_id=ObjectId(coverage_id) if coverage_id else None,
            get_all=True if get_all or get_all == "true" else False,
        )
        return database_interface.convert_to_api_type(coverages), 200
    except MissingInputParameters:
        return "invalid input parameters", 400
    except NotFound:
        return "coverage not found", 404


def coverage_options():  # noqa: E501
    """coverage_options

     # noqa: E501


    :rtype: None
    """
    return '''Options:
    get: supply coverageID or referenceName and (commitID or 
         (commitHash and (projectID or projectUrl or projectTitle))) as query parameter
         for example [API_URL]/commit?projectTitle=TestProject&commitHash=eft6stn
    post: supply the correct coverageType as json body
         use the xml_parser for this, to create it from xml coverage
        and commitID or (commitHash and (projectUrl or projectTitle or projectId)) as query parameter
        for example [API_URL]/commit?projectTitle=TestProject
    put: missing implementation!
    patch: missing implementation!
    delete: missing implementation!
    ''', 200


def coverage_patch(project_url=None, project_title=None, project_id=None, commit_hash=None, commit_id=None, job_reference=None, description=None, coverage_id=None):  # noqa: E501
    """modiefies Coverage

     # noqa: E501

    :param project_url: 
    :type project_url: str
    :param project_title: 
    :type project_title: str
    :param project_id: 
    :type project_id: str
    :param commit_hash: 
    :type commit_hash: str
    :param commit_id: 
    :type commit_id: str
    :param job_reference: 
    :type job_reference: str
    :param description: 
    :type description: str
    :param coverage_id: 
    :type coverage_id: str

    :rtype: ObjectID
    """
    return "not implemented", 501


def coverage_post(body=None, project_url=None, project_title=None, project_id=None, commit_hash=None, commit_id=None):  # noqa: E501
    """adds coverage to Database

     # noqa: E501

    :param body: Coverage json
    :type body: dict | bytes
    :param project_url: 
    :type project_url: str
    :param project_title: 
    :type project_title: str
    :param project_id: 
    :type project_id: str
    :param commit_hash: 
    :type commit_hash: str
    :param commit_id: 
    :type commit_id: str

    :rtype: ObjectID
    """
    if connexion.request.is_json:
        coverage = CoverageType.from_dict(connexion.request.get_json())  # noqa: E501
        try:
            coverage_id = database_interface.write_coverage(
                project_url=project_url,
                project_title=project_title,
                project_id=ObjectId(project_id) if project_id else None,
                commit_hash=commit_hash,
                commit_id=ObjectId(commit_id) if commit_id else None,
                coverage=database_interface.convert_from_api_type(coverage))
            return ObjectID(id=str(coverage_id)), 201
        except MissingInputParameters:
            return "invalid input parameters", 400
        except NotFound:
            return "project or commit does not exist", 404
        except NotUnique:
            return "duplicate coverage", 409
    else:
        return "invalid post body, has to be json", 400


def coverage_put(body=None, project_url=None, project_title=None, project_id=None, commit_hash=None, commit_id=None, job_reference=None, description=None, coverage_id=None):  # noqa: E501
    """replaces Coverage

     # noqa: E501

    :param body: Coverage json
    :type body: dict | bytes
    :param project_url: 
    :type project_url: str
    :param project_title: 
    :type project_title: str
    :param project_id: 
    :type project_id: str
    :param commit_hash: 
    :type commit_hash: str
    :param commit_id: 
    :type commit_id: str
    :param job_reference: 
    :type job_reference: str
    :param description: 
    :type description: str
    :param coverage_id: 
    :type coverage_id: str

    :rtype: ObjectID
    """
    if connexion.request.is_json:
        body = CoverageType.from_dict(connexion.request.get_json())  # noqa: E501
    return "not implemented", 501
