import connexion
import six

from bson.objectid import ObjectId

from ..models.commit_type import CommitType  # noqa: E501
from ..models.object_id import ObjectID  # noqa: E501
from .. import util
from ...database import database_interface
from ...database.errors import NotFound, NotUnique, NotAuthorized, MissingInputParameters


def commit_delete(project_url=None, project_title=None, project_id=None, commit_hash=None, commit_id=None):  # noqa: E501
    """deletes commit

     # noqa: E501

    :param project_url: 
    :type project_url: str
    :param project_title: 
    :type project_title: str
    :param project_id: 
    :type project_id: str
    :param commit_hash: 
    :type commit_hash: str
    :param commit_id: 
    :type commit_id: str

    :rtype: ObjectID
    """
    return "not implemented", 501


def commit_get(get_all=None, project_url=None, project_title=None, project_id=None, commit_hash=None, commit_id=None):  # noqa: E501
    """return commit

     # noqa: E501

    :param get_all:
    :type get_all: bool
    :param project_url: 
    :type project_url: str
    :param project_title: 
    :type project_title: str
    :param project_id: 
    :type project_id: str
    :param commit_hash: 
    :type commit_hash: str
    :param commit_id: 
    :type commit_id: str

    :rtype: List[CommitType]
    """
    try:
        commits = database_interface.read_commit(
            project_url=project_url,
            project_title=project_title,
            project_id=ObjectId(project_id) if project_id else None,
            commit_hash=commit_hash,
            commit_id=ObjectId(commit_id) if commit_id else None,
            get_all=True if get_all or get_all == "true" else False,
        )
        return database_interface.convert_to_api_type(commits), 200
    except MissingInputParameters:
        return "invalid input parameters", 400
    except NotFound:
        return "commit not found", 404


def commit_options():  # noqa: E501
    """commit_options

     # noqa: E501


    :rtype: None
    """
    return '''Options:
    get: supply commitID or (commitHash and (projectID or projectUrl or projectTitle)) as query parameter
         for example [API_URL]/commit?projectTitle=TestProject&commitHash=eft6stn
    post: supply the correct CommitType as json body
         for example {"commit_hash"="eft6stn", 
                     "message": "this is a test commit", 
                     "branch": "master"}
        and projectUrl or projectTitle or projectId as query parameter
        for example [API_URL]/commit?projectTitle=TestProject
    put: missing implementation!
    patch: missing implementation!
    delete: missing implementation!
    ''', 200


def commit_patch(project_url=None, project_title=None, project_id=None, commit_hash=None, commit_id=None):  # noqa: E501
    """modifies commit

     # noqa: E501

    :param project_url: 
    :type project_url: str
    :param project_title: 
    :type project_title: str
    :param project_id: 
    :type project_id: str
    :param commit_hash: 
    :type commit_hash: str
    :param commit_id: 
    :type commit_id: str

    :rtype: ObjectID
    """
    return "not implemented", 501


def commit_post(body=None, project_url=None, project_title=None, project_id=None):  # noqa: E501
    """adds commit to database

     # noqa: E501

    :param body: Commit Object
    :type body: dict | bytes
    :param project_url: 
    :type project_url: str
    :param project_title: 
    :type project_title: str
    :param project_id: 
    :type project_id: str

    :rtype: ObjectID
    """
    if connexion.request.is_json:
        commit = CommitType.from_dict(connexion.request.get_json())  # noqa: E501
        try:
            commit_id = database_interface.write_commit(
                project_url=project_url,
                project_title=project_title,
                project_id=ObjectId(project_id) if project_id else None,
                commit=database_interface.convert_from_api_type(commit))
            return ObjectID(id=str(commit_id)), 201
        except MissingInputParameters:
            return "invalid input parameters", 400
        except NotFound:
            return "project not found", 404
        except NotUnique:
            return "duplicate commit, has to be unique commit_hash", 409
    else:
        return "invalid post body, has to be json", 400


def commit_put(body=None, project_url=None, project_title=None, project_id=None, commit_hash=None, commit_id=None):  # noqa: E501
    """replaces commit

     # noqa: E501

    :param body: Commit Object
    :type body: dict | bytes
    :param project_url: 
    :type project_url: str
    :param project_title: 
    :type project_title: str
    :param project_id: 
    :type project_id: str
    :param commit_hash: 
    :type commit_hash: str
    :param commit_id: 
    :type commit_id: str

    :rtype: ObjectID
    """
    if connexion.request.is_json:
        body = CommitType.from_dict(connexion.request.get_json())  # noqa: E501
    return "not implemented", 501
