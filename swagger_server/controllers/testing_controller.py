import connexion
import six

from .. import util


def root_get():  # noqa: E501
    """root_get

    test API availability # noqa: E501


    :rtype: None
    """
    return 'do some magic!'
