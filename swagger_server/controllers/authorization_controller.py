import os

from typing import List
"""
controller generated to handled auth operation described at:
https://connexion.readthedocs.io/en/latest/security.html
"""


def check_ModifyApiKeyAuth(api_key, required_scopes):
    if api_key == os.getenv("MODIFY_API_KEY"):
        return {'MODIFY-API-KEY': api_key}


