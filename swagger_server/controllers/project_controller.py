import connexion
import six

from bson.objectid import ObjectId

from ..models.object_id import ObjectID  # noqa: E501
from ..models.project_type import ProjectType  # noqa: E501
from .. import util
from ...database import database_interface
from ...database.errors import NotFound, NotUnique, NotAuthorized, MissingInputParameters


def project_delete(project_url=None, project_title=None, project_id=None):  # noqa: E501
    """deletes project

     # noqa: E501

    :param project_url: 
    :type project_url: str
    :param project_title: 
    :type project_title: str
    :param project_id: 
    :type project_id: str

    :rtype: ObjectID
    """
    return "not implemented", 501


def project_get(get_all=None, project_url=None, project_title=None, project_id=None):  # noqa: E501
    """returns project

     # noqa: E501

    :param get_all:
    :type get_all: bool
    :param project_url: 
    :type project_url: str
    :param project_title: 
    :type project_title: str
    :param project_id: 
    :type project_id: str

    :rtype: List[ProjectType]
    """
    try:
        projects = database_interface.read_project(
            project_url=project_url,
            project_title=project_title,
            project_id=ObjectId(project_id) if project_id else None,
            get_all=True if get_all or get_all == "true" else False,
        )
        return database_interface.convert_to_api_type(projects), 200
    except MissingInputParameters:
        return "invalid input parameters", 400
    except NotFound:
        return "project not found", 404


def project_options():  # noqa: E501
    """project_options

     # noqa: E501


    :rtype: None
    """
    return '''Options:
    get: supply projectUrl or projectTitle or projectId as query parameter
         for example [API_URL]/project?projectTitle=TestProject
    post: supply the correct ProjectType as json body
         for example {"url": "https://Test_Project.com", 
                     "title": "TestProject", 
                     "description": "Test"}
    put: missing implementation!
    patch: missing implementation!
    delete: missing implementation!
    ''', 200


def project_patch(project_url=None, project_title=None, project_id=None):  # noqa: E501
    """modifies project

     # noqa: E501

    :param project_url: 
    :type project_url: str
    :param project_title: 
    :type project_title: str
    :param project_id: 
    :type project_id: str

    :rtype: ObjectID
    """
    return "not implemented", 501


def project_post(body=None):  # noqa: E501
    """adds Project to Database

     # noqa: E501

    :param body: Project Object
    :type body: dict | bytes

    :rtype: ObjectID
    """
    if connexion.request.is_json:
        project = ProjectType.from_dict(connexion.request.get_json())  # noqa: E501
        try:
            project_id = database_interface.write_project(
                database_interface.convert_from_api_type(project)
            )
            return ObjectID(id=str(project_id)), 201
        except NotUnique:
            return "duplicate project, has to be unique title and url", 409
    else:
        return "invalid post body, has to be json", 400


def project_put(body=None, project_url=None, project_title=None, project_id=None):  # noqa: E501
    """replaces project

     # noqa: E501

    :param body: Project Object
    :type body: dict | bytes
    :param project_url: 
    :type project_url: str
    :param project_title: 
    :type project_title: str
    :param project_id: 
    :type project_id: str

    :rtype: ObjectID
    """
    if connexion.request.is_json:
        body = ProjectType.from_dict(connexion.request.get_json())  # noqa: E501
    return "not implemented", 501
