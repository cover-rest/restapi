import os
import logging

import connexion
from flask_testing import TestCase

from ..encoder import JSONEncoder


class BaseTestCase(TestCase):

    def create_app(self):
        os.environ['MODIFY_API_KEY'] = 'test_api_key'
        logging.getLogger('connexion.operation').setLevel('DEBUG')
        app = connexion.App(__name__, specification_dir='../swagger/')
        app.app.json_encoder = JSONEncoder
        app.add_api('swagger.yaml', arguments={'title': 'Cover-Rest Interface API'}, pythonic_params=True)
        return app.app
