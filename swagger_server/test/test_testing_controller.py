# coding: utf-8

from __future__ import absolute_import

from flask import json
from six import BytesIO

from ..test import BaseTestCase


class TestTestingController(BaseTestCase):
    """TestingController integration test stubs"""

    def test_root_get(self):
        """Test case for root_get

        
        """
        response = self.client.open(
            '/Cover-Rest/Interface-API/1.2.3/',
            method='GET')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))


if __name__ == '__main__':
    import unittest
    unittest.main()
