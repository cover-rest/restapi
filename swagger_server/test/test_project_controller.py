# coding: utf-8

from __future__ import absolute_import

import mongoengine
from flask import json
from six import BytesIO
from bson.objectid import ObjectId

from ..models.project_type import ProjectType  # noqa: E501
from ..test import BaseTestCase
from ...database.types.project_type import ProjectDatabaseType
from ...database.database_interface import write_project

mongoengine.disconnect()
db = mongoengine.connect(host="mongodb://127.0.0.1:27017/cover_rest_test_db")
db.drop_database("cover_rest_test_db")


class TestProjectController(BaseTestCase):
    """ProjectController integration test stubs"""

    def test_project_delete(self):
        """Test case for project_delete

        deletes project
        """
        query_options = [[('projectUrl', 'project_url_example')],
                         [('projectTitle', 'project_title_example')],
                         [('projectID', 'project_id_example')]]
        for item in query_options:
            response = self.client.open(
                '/Cover-Rest/Interface-API/1.2.3/project',
                method='DELETE',
                query_string=item,
                headers={"MODIFY-API-KEY": "test_api_key"})
            self.assertStatus(response, 501,
                              f'{item} delete failed. Response body is : ' + response.data.decode('utf-8'))

    def test_project_get(self):
        """Test case for project_get

        returns project
        """
        db = mongoengine.get_connection()
        db.drop_database("cover_rest_test_db")

        project_id = write_project(ProjectDatabaseType(url="http://test-project.com",
                                                       title="testProject",
                                                       description="test Project"))
        self.assertIsInstance(project_id, ObjectId, "Project write using database interface failed")

        query_options = [[('getAll', True)],
                         [('projectUrl', "http://test-project.com")],
                         [('projectTitle', "testProject")],
                         [('projectID', str(project_id))]]
        for item in query_options:
            response = self.client.open(
                '/Cover-Rest/Interface-API/1.2.3/project',
                method='GET',
                query_string=item
            )
            self.assert200(response,
                           f'{item} read failed. Response body is : ' + response.data.decode('utf-8'))

    def test_project_options(self):
        """Test case for project_options


        """
        response = self.client.open(
            '/Cover-Rest/Interface-API/1.2.3/project',
            method='OPTIONS')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_project_patch(self):
        """Test case for project_patch

        modifies project
        """
        query_options = [[('projectUrl', 'project_url_example')],
                         [('projectTitle', 'project_title_example')],
                         [('projectID', 'project_id_example')]]
        for item in query_options:
            response = self.client.open(
                '/Cover-Rest/Interface-API/1.2.3/project',
                method='PATCH',
                query_string=item,
                headers={"MODIFY-API-KEY": "test_api_key"})
            self.assertStatus(response, 501,
                              f'{item} patch failed. Response body is : ' + response.data.decode('utf-8'))

    def test_project_post(self):
        """Test case for project_post

        adds Project to Database
        """
        db = mongoengine.get_connection()
        db.drop_database("cover_rest_test_db")

        body = ProjectType(url="http://test-project.com", title="testProject", description="test Project")
        response = self.client.open(
            '/Cover-Rest/Interface-API/1.2.3/project',
            method='POST',
            data=json.dumps(body),
            content_type='application/json',
            headers={"MODIFY-API-KEY": "test_api_key"})
        self.assertStatus(response, 201,
                          'Response body is : ' + response.data.decode('utf-8'))

    def test_project_put(self):
        """Test case for project_put

        replaces project
        """
        body = ProjectType(url="http://test-project.com", title="testProject", description="test Project")
        query_options = [[('projectUrl', 'project_url_example')],
                         [('projectTitle', 'project_title_example')],
                         [('projectID', 'project_id_example')]]
        for item in query_options:
            response = self.client.open(
                '/Cover-Rest/Interface-API/1.2.3/project',
                method='PUT',
                data=json.dumps(body),
                content_type='application/json',
                query_string=item,
                headers={"MODIFY-API-KEY": "test_api_key"})
            self.assertStatus(response, 501,
                              f'{item} put failed. Response body is : ' + response.data.decode('utf-8'))


if __name__ == '__main__':
    import unittest

    unittest.main()
