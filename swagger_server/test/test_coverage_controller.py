# coding: utf-8

from __future__ import absolute_import

import mongoengine

from flask import json
from six import BytesIO
from bson.objectid import ObjectId

from ..models.coverage_type import CoverageType  # noqa: E501
from ..models.object_id import ObjectID  # noqa: E501
from ..test import BaseTestCase
from ...database.types.project_type import ProjectDatabaseType
from ...database.database_interface import write_project
from ...database.types.commit_type import CommitDatabaseType
from ...database.database_interface import write_commit
from ...database.database_interface import write_coverage
from ...database.database_interface import convert_from_api_type
from ...parser.xml_parse import XmlParse

mongoengine.disconnect()
db = mongoengine.connect(host="mongodb://127.0.0.1:27017/cover_rest_test_db")
db.drop_database("cover_rest_test_db")

dataType = XmlParse.from_path("coverage_python.xml")


class TestCoverageController(BaseTestCase):
    """CoverageController integration test stubs"""

    def test_coverage_delete(self):
        """Test case for coverage_delete

        deletes Coverage
        """
        query_options = [[('projectUrl', 'project_url_example'), ('commitHash', 'commit_hash_example'),
                          ('jobReference', 'job_reference_example')],
                         [('projectTitle', 'project_title_example'), ('commitHash', 'commit_hash_example'),
                          ('jobReference', 'job_reference_example')],
                         [('projectID', 'project_id_example'), ('commitHash', 'commit_hash_example'),
                          ('jobReference', 'job_reference_example')],
                         [('commitID', 'commit_id_example'), ('jobReference', 'job_reference_example')],
                         [('coverageID', 'coverage_id_example')]]
        for item in query_options:
            response = self.client.open(
                '/Cover-Rest/Interface-API/1.2.3/coverage',
                method='DELETE',
                query_string=item,
                headers={"MODIFY-API-KEY": "test_api_key"})
            self.assertStatus(response, 501,
                              f'{item} delete failed. Response body is : ' + response.data.decode('utf-8'))

    def test_coverage_get(self):
        """Test case for coverage_get

        returns Coverage
        """
        db = mongoengine.get_connection()
        db.drop_database("cover_rest_test_db")

        project_id = write_project(ProjectDatabaseType(url="http://test-project.com",
                                                       title="testProject",
                                                       description="test Project"))
        self.assertIsInstance(project_id, ObjectId, "Project write using database interface failed")

        commit_id = write_commit(project_id=project_id, commit=CommitDatabaseType(commit_hash="testCommit",
                                                                                  message="test Commit",
                                                                                  branch="master"))
        self.assertIsInstance(commit_id, ObjectId, "Commit write using database interface failed")

        dataType.coverage.job_reference = "testCoverage"
        coverage_id = write_coverage(commit_id=commit_id,
                                     coverage=convert_from_api_type(CoverageType.from_dict(dataType.to_dict())))
        self.assertIsInstance(coverage_id, ObjectId, "Coverage write using database interface failed")

        query_options = [[('projectUrl', "http://test-project.com"), ('commitHash', "testCommit"),
                          ('getAll', True)],
                         [('projectTitle', "testProject"), ('commitHash', "testCommit"),
                          ('getAll', True)],
                         [('projectID', str(project_id)), ('commitHash', "testCommit"),
                          ('getAll', True)],
                         [('projectUrl', "http://test-project.com"), ('commitHash', "testCommit"),
                          ('jobReference', "testCoverage")],
                         [('projectTitle', "testProject"), ('commitHash', "testCommit"),
                          ('jobReference', "testCoverage")],
                         [('projectID', str(project_id)), ('commitHash', "testCommit"),
                          ('jobReference', "testCoverage")],
                         [('commitID', str(commit_id)), ('getAll', True)],
                         [('commitID', str(commit_id)), ('jobReference', "testCoverage")],
                         [('coverageID', str(coverage_id))]]
        for item in query_options:
            response = self.client.open(
                '/Cover-Rest/Interface-API/1.2.3/coverage',
                method='GET',
                query_string=item)
            self.assert200(response,
                           f'{item} get failed. Response body is : ' + response.data.decode('utf-8'))

    def test_coverage_options(self):
        """Test case for coverage_options

        
        """
        response = self.client.open(
            '/Cover-Rest/Interface-API/1.2.3/coverage',
            method='OPTIONS')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_coverage_patch(self):
        """Test case for coverage_patch

        modiefies Coverage
        """
        query_options = [[('projectUrl', 'project_url_example'), ('commitHash', 'commit_hash_example'),
                          ('jobReference', 'job_reference_example')],
                         [('projectTitle', 'project_title_example'), ('commitHash', 'commit_hash_example'),
                          ('jobReference', 'job_reference_example')],
                         [('projectID', 'project_id_example'), ('commitHash', 'commit_hash_example'),
                          ('jobReference', 'job_reference_example')],
                         [('commitID', 'commit_id_example'), ('jobReference', 'job_reference_example')],
                         [('coverageID', 'coverage_id_example')]]
        for item in query_options:
            response = self.client.open(
                '/Cover-Rest/Interface-API/1.2.3/coverage',
                method='PATCH',
                query_string=item,
                headers={"MODIFY-API-KEY": "test_api_key"})
            self.assertStatus(response, 501,
                              f'{item} patch failed. Response body is : ' + response.data.decode('utf-8'))

    def test_coverage_post(self):
        """Test case for coverage_post

        adds coverage to Database
        """
        db = mongoengine.get_connection()
        db.drop_database("cover_rest_test_db")

        project_id = write_project(ProjectDatabaseType(url="http://test-project.com",
                                                       title="testProject",
                                                       description="test Project"))
        self.assertIsInstance(project_id, ObjectId, "Project write using database interface failed")

        commit_id = write_commit(project_id=project_id, commit=CommitDatabaseType(commit_hash="testCommit",
                                                                                  message="test Commit",
                                                                                  branch="master"))
        self.assertIsInstance(commit_id, ObjectId, "Commit write using database interface failed")

        query_options = [[('projectUrl', "http://test-project.com"), ('commitHash', "testCommit")],
                         [('projectTitle', "testProject"), ('commitHash', "testCommit")],
                         [('projectID', str(project_id)), ('commitHash', "testCommit")],
                         [('commitID', str(commit_id))]]
        for index, item in enumerate(query_options):
            dataType.coverage.job_reference = f"testCoverage{index}"
            body = CoverageType.from_dict(dataType.to_dict())
            response = self.client.open(
                '/Cover-Rest/Interface-API/1.2.3/coverage',
                method='POST',
                data=json.dumps(body),
                content_type='application/json',
                query_string=item,
                headers={"MODIFY-API-KEY": "test_api_key"})
            self.assertStatus(response, 201,
                              f'{item}, post failed. Response body is : ' + response.data.decode('utf-8'))
        db.drop_database("cover_rest_test_db")

    def test_coverage_put(self):
        """Test case for coverage_put

        replaces Coverage
        """
        db = mongoengine.get_connection()
        db.drop_database("cover_rest_test_db")

        dataType.coverage.job_reference = f"testCoverage"
        body = CoverageType.from_dict(dataType.to_dict())
        query_options = [[('projectUrl', 'project_url_example'), ('commitHash', 'commit_hash_example'),
                          ('jobReference', 'job_reference_example')],
                         [('projectTitle', 'project_title_example'), ('commitHash', 'commit_hash_example'),
                          ('jobReference', 'job_reference_example')],
                         [('projectID', 'project_id_example'), ('commitHash', 'commit_hash_example'),
                          ('jobReference', 'job_reference_example')],
                         [('commitID', 'commit_id_example'), ('jobReference', 'job_reference_example')],
                         [('coverageID', 'coverage_id_example')]]
        for item in query_options:
            response = self.client.open(
                '/Cover-Rest/Interface-API/1.2.3/coverage',
                method='PUT',
                data=json.dumps(body),
                content_type='application/json',
                query_string=item,
                headers={"MODIFY-API-KEY": "test_api_key"})
            self.assertStatus(response, 501,
                              f'{item} put failed. Response body is : ' + response.data.decode('utf-8'))


if __name__ == '__main__':
    import unittest

    unittest.main()
