# coding: utf-8

from __future__ import absolute_import

import mongoengine

from flask import json
from six import BytesIO
from bson.objectid import ObjectId

from ..models.commit_type import CommitType  # noqa: E501
from ..models.object_id import ObjectID  # noqa: E501
from ..test import BaseTestCase
from ...database.types.project_type import ProjectDatabaseType
from ...database.database_interface import write_project
from ...database.types.commit_type import CommitDatabaseType
from ...database.database_interface import write_commit

mongoengine.disconnect()
db = mongoengine.connect(host="mongodb://127.0.0.1:27017/cover_rest_test_db")
db.drop_database("cover_rest_test_db")


class TestCommitController(BaseTestCase):
    """CommitController integration test stubs"""

    def test_commit_delete(self):
        """Test case for commit_delete

        deletes commit
        """
        query_options = [[('projectUrl', 'project_url_example'), ('commitHash', 'commit_hash_example')],
                         [('projectTitle', 'project_title_example'), ('commitHash', 'commit_hash_example')],
                         [('projectID', 'project_id_example'), ('commitHash', 'commit_hash_example')],
                         [('commitID', 'commit_id_example')]]
        for item in query_options:
            response = self.client.open(
                '/Cover-Rest/Interface-API/1.2.3/commit',
                method='DELETE',
                query_string=item,
                headers={"MODIFY-API-KEY": "test_api_key"})
            self.assertStatus(response, 501,
                              f'{item} delete failed. Response body is : ' + response.data.decode('utf-8'))

    def test_commit_get(self):
        """Test case for commit_get

        return commit
        """
        db = mongoengine.get_connection()
        db.drop_database("cover_rest_test_db")

        project_id = write_project(ProjectDatabaseType(url="http://test-project.com",
                                                       title="testProject",
                                                       description="test Project"))
        self.assertIsInstance(project_id, ObjectId, "Project write using database interface failed")

        commit_id = write_commit(project_id=project_id, commit=CommitDatabaseType(commit_hash="testCommit",
                                                                                  message="test Commit",
                                                                                  branch="master"))
        self.assertIsInstance(commit_id, ObjectId, "Commit write using database interface failed")

        query_options = [[('projectUrl', "http://test-project.com"), ('getAll', True)],
                         [('projectTitle', "testProject"), ('getAll', True)],
                         [('projectID', str(project_id)), ('getAll', True)],
                         [('projectUrl', "http://test-project.com"), ('commitHash', "testCommit")],
                         [('projectTitle', "testProject"), ('commitHash', "testCommit")],
                         [('projectID', str(project_id)), ('commitHash', "testCommit")],
                         [('commitID', str(commit_id))]]
        for item in query_options:
            response = self.client.open(
                '/Cover-Rest/Interface-API/1.2.3/commit',
                method='GET',
                query_string=item)
            self.assert200(response,
                           f'{item} get failed. Response body is : ' + response.data.decode('utf-8'))

    def test_commit_options(self):
        """Test case for commit_options

        
        """
        response = self.client.open(
            '/Cover-Rest/Interface-API/1.2.3/commit',
            method='OPTIONS')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_commit_patch(self):
        """Test case for commit_patch

        modifies commit
        """
        query_options = [[('projectUrl', 'project_url_example'), ('commitHash', 'commit_hash_example')],
                         [('projectTitle', 'project_title_example'), ('commitHash', 'commit_hash_example')],
                         [('projectID', 'project_id_example'), ('commitHash', 'commit_hash_example')],
                         [('commitID', 'commit_id_example')]]
        for item in query_options:
            response = self.client.open(
                '/Cover-Rest/Interface-API/1.2.3/commit',
                method='PATCH',
                query_string=item,
                headers={"MODIFY-API-KEY": "test_api_key"})
            self.assertStatus(response, 501,
                              f'{item} patch failed. Response body is : ' + response.data.decode('utf-8'))

    def test_commit_post(self):
        """Test case for commit_post

        adds commit to database
        """
        db = mongoengine.get_connection()
        db.drop_database("cover_rest_test_db")

        body = CommitType(commit_hash="testCommit", message="test Commit", branch="master")
        query_options = [[('projectUrl', "http://test-project.com")],
                         [('projectTitle', "testProject")],
                         [('projectID', 'project_id_example')]]
        for item in query_options:
            project_id = write_project(ProjectDatabaseType(url="http://test-project.com",
                                                           title="testProject",
                                                           description="test Project"))
            if item == query_options[-1]:  # TODO find better solution
                item = [('projectID', str(project_id))]
            self.assertIsInstance(project_id, ObjectId, "Project write using database interface failed")
            response = self.client.open(
                '/Cover-Rest/Interface-API/1.2.3/commit',
                method='POST',
                data=json.dumps(body),
                content_type='application/json',
                query_string=item,
                headers={"MODIFY-API-KEY": "test_api_key"})
            db.drop_database("cover_rest_test_db")
            self.assertStatus(response, 201,
                              f'{item}, post failed. Response body is : ' + response.data.decode('utf-8'))

    def test_commit_put(self):
        """Test case for commit_put

        replaces commit
        """
        body = CommitType(commit_hash="testCommit", message="test Commit", branch="master")
        query_options = [[('projectUrl', 'project_url_example'), ('commitHash', 'commit_hash_example')],
                         [('projectTitle', 'project_title_example'), ('commitHash', 'commit_hash_example')],
                         [('projectID', 'project_id_example'), ('commitHash', 'commit_hash_example')],
                         [('commitID', 'commit_id_example')]]
        for item in query_options:
            response = self.client.open(
                '/Cover-Rest/Interface-API/1.2.3/commit',
                method='PUT',
                data=json.dumps(body),
                content_type='application/json',
                query_string=item,
                headers={"MODIFY-API-KEY": "test_api_key"})
            self.assertStatus(response, 501,
                              f'{item} put failed. Response body is : ' + response.data.decode('utf-8'))


if __name__ == '__main__':
    import unittest

    unittest.main()
