# coding: utf-8

from __future__ import absolute_import
from datetime import date, datetime  # noqa: F401

from typing import List, Dict  # noqa: F401

from .base_model_ import Model
from .line_type_conditions import LineTypeConditions  # noqa: F401,E501
from .. import util


class LineType(Model):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    def __init__(self, number: str=None, hits: str=None, branch: str=None, condition_coverage: str=None, conditions: List[LineTypeConditions]=None):  # noqa: E501
        """LineType - a model defined in Swagger

        :param number: The number of this LineType.  # noqa: E501
        :type number: str
        :param hits: The hits of this LineType.  # noqa: E501
        :type hits: str
        :param branch: The branch of this LineType.  # noqa: E501
        :type branch: str
        :param condition_coverage: The condition_coverage of this LineType.  # noqa: E501
        :type condition_coverage: str
        :param conditions: The conditions of this LineType.  # noqa: E501
        :type conditions: List[LineTypeConditions]
        """
        self.swagger_types = {
            'number': str,
            'hits': str,
            'branch': str,
            'condition_coverage': str,
            'conditions': List[LineTypeConditions]
        }

        self.attribute_map = {
            'number': 'number',
            'hits': 'hits',
            'branch': 'branch',
            'condition_coverage': 'condition_coverage',
            'conditions': 'conditions'
        }
        self._number = number
        self._hits = hits
        self._branch = branch
        self._condition_coverage = condition_coverage
        self._conditions = conditions

    @classmethod
    def from_dict(cls, dikt) -> 'LineType':
        """Returns the dict as a model

        :param dikt: A dict.
        :type: dict
        :return: The LineType of this LineType.  # noqa: E501
        :rtype: LineType
        """
        return util.deserialize_model(dikt, cls)

    @property
    def number(self) -> str:
        """Gets the number of this LineType.


        :return: The number of this LineType.
        :rtype: str
        """
        return self._number

    @number.setter
    def number(self, number: str):
        """Sets the number of this LineType.


        :param number: The number of this LineType.
        :type number: str
        """
        if number is None:
            raise ValueError("Invalid value for `number`, must not be `None`")  # noqa: E501

        self._number = number

    @property
    def hits(self) -> str:
        """Gets the hits of this LineType.


        :return: The hits of this LineType.
        :rtype: str
        """
        return self._hits

    @hits.setter
    def hits(self, hits: str):
        """Sets the hits of this LineType.


        :param hits: The hits of this LineType.
        :type hits: str
        """
        if hits is None:
            raise ValueError("Invalid value for `hits`, must not be `None`")  # noqa: E501

        self._hits = hits

    @property
    def branch(self) -> str:
        """Gets the branch of this LineType.


        :return: The branch of this LineType.
        :rtype: str
        """
        return self._branch

    @branch.setter
    def branch(self, branch: str):
        """Sets the branch of this LineType.


        :param branch: The branch of this LineType.
        :type branch: str
        """

        self._branch = branch

    @property
    def condition_coverage(self) -> str:
        """Gets the condition_coverage of this LineType.


        :return: The condition_coverage of this LineType.
        :rtype: str
        """
        return self._condition_coverage

    @condition_coverage.setter
    def condition_coverage(self, condition_coverage: str):
        """Sets the condition_coverage of this LineType.


        :param condition_coverage: The condition_coverage of this LineType.
        :type condition_coverage: str
        """

        self._condition_coverage = condition_coverage

    @property
    def conditions(self) -> List[LineTypeConditions]:
        """Gets the conditions of this LineType.


        :return: The conditions of this LineType.
        :rtype: List[LineTypeConditions]
        """
        return self._conditions

    @conditions.setter
    def conditions(self, conditions: List[LineTypeConditions]):
        """Sets the conditions of this LineType.


        :param conditions: The conditions of this LineType.
        :type conditions: List[LineTypeConditions]
        """

        self._conditions = conditions
