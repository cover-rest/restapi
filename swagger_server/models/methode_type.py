# coding: utf-8

from __future__ import absolute_import
from datetime import date, datetime  # noqa: F401

from typing import List, Dict  # noqa: F401

from .base_model_ import Model
from .line_type import LineType  # noqa: F401,E501
from .. import util


class MethodeType(Model):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    def __init__(self, name: str=None, filename: str=None, complexity: float=None, line_rate: float=None, branch_rate: float=None, lines: List[LineType]=None):  # noqa: E501
        """MethodeType - a model defined in Swagger

        :param name: The name of this MethodeType.  # noqa: E501
        :type name: str
        :param filename: The filename of this MethodeType.  # noqa: E501
        :type filename: str
        :param complexity: The complexity of this MethodeType.  # noqa: E501
        :type complexity: float
        :param line_rate: The line_rate of this MethodeType.  # noqa: E501
        :type line_rate: float
        :param branch_rate: The branch_rate of this MethodeType.  # noqa: E501
        :type branch_rate: float
        :param lines: The lines of this MethodeType.  # noqa: E501
        :type lines: List[LineType]
        """
        self.swagger_types = {
            'name': str,
            'filename': str,
            'complexity': float,
            'line_rate': float,
            'branch_rate': float,
            'lines': List[LineType]
        }

        self.attribute_map = {
            'name': 'name',
            'filename': 'filename',
            'complexity': 'complexity',
            'line_rate': 'line_rate',
            'branch_rate': 'branch_rate',
            'lines': 'lines'
        }
        self._name = name
        self._filename = filename
        self._complexity = complexity
        self._line_rate = line_rate
        self._branch_rate = branch_rate
        self._lines = lines

    @classmethod
    def from_dict(cls, dikt) -> 'MethodeType':
        """Returns the dict as a model

        :param dikt: A dict.
        :type: dict
        :return: The MethodeType of this MethodeType.  # noqa: E501
        :rtype: MethodeType
        """
        return util.deserialize_model(dikt, cls)

    @property
    def name(self) -> str:
        """Gets the name of this MethodeType.


        :return: The name of this MethodeType.
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name: str):
        """Sets the name of this MethodeType.


        :param name: The name of this MethodeType.
        :type name: str
        """
        if name is None:
            raise ValueError("Invalid value for `name`, must not be `None`")  # noqa: E501

        self._name = name

    @property
    def filename(self) -> str:
        """Gets the filename of this MethodeType.


        :return: The filename of this MethodeType.
        :rtype: str
        """
        return self._filename

    @filename.setter
    def filename(self, filename: str):
        """Sets the filename of this MethodeType.


        :param filename: The filename of this MethodeType.
        :type filename: str
        """
        if filename is None:
            raise ValueError("Invalid value for `filename`, must not be `None`")  # noqa: E501

        self._filename = filename

    @property
    def complexity(self) -> float:
        """Gets the complexity of this MethodeType.


        :return: The complexity of this MethodeType.
        :rtype: float
        """
        return self._complexity

    @complexity.setter
    def complexity(self, complexity: float):
        """Sets the complexity of this MethodeType.


        :param complexity: The complexity of this MethodeType.
        :type complexity: float
        """

        self._complexity = complexity

    @property
    def line_rate(self) -> float:
        """Gets the line_rate of this MethodeType.


        :return: The line_rate of this MethodeType.
        :rtype: float
        """
        return self._line_rate

    @line_rate.setter
    def line_rate(self, line_rate: float):
        """Sets the line_rate of this MethodeType.


        :param line_rate: The line_rate of this MethodeType.
        :type line_rate: float
        """

        self._line_rate = line_rate

    @property
    def branch_rate(self) -> float:
        """Gets the branch_rate of this MethodeType.


        :return: The branch_rate of this MethodeType.
        :rtype: float
        """
        return self._branch_rate

    @branch_rate.setter
    def branch_rate(self, branch_rate: float):
        """Sets the branch_rate of this MethodeType.


        :param branch_rate: The branch_rate of this MethodeType.
        :type branch_rate: float
        """

        self._branch_rate = branch_rate

    @property
    def lines(self) -> List[LineType]:
        """Gets the lines of this MethodeType.


        :return: The lines of this MethodeType.
        :rtype: List[LineType]
        """
        return self._lines

    @lines.setter
    def lines(self, lines: List[LineType]):
        """Sets the lines of this MethodeType.


        :param lines: The lines of this MethodeType.
        :type lines: List[LineType]
        """

        self._lines = lines
